#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Annotation represent a start and a stop character positions, and a label (a string).
"""

from pydantic import BaseModel, validator

class Annotation(BaseModel):
    start: int
    stop: int
    label: str
    
    @validator('label')
    def normalise_label(cls, label):
        nlabel = label.lower()
        nlabel = '_'.join(nlabel.split())
        return nlabel
